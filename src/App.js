import React, { Component } from 'react';
import './App.css';


import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Index from './components/Index.js';
import ReactGA from 'react-ga';


class App extends Component {
  render() {
    this.api_base = "http://localhost:5000/";

    ReactGA.initialize('UA-178951778-1');
    return (

      <BrowserRouter>
        <Switch>

          <Route render={(props) => <Index />} />
        </Switch>
      </BrowserRouter>
    );
  }
}

export default App;