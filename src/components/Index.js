import React, { Component, useState, useEffect } from 'react';
import ReactGA from 'react-ga';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar'
import IconButton from '@material-ui/core/IconButton'
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'
import 'typeface-roboto';

import {ThemeProvider} from "styled-components";
import { lightTheme, darkTheme } from "./Themes"

class Index extends Component {

    constructor(props) {
        super(props);
   
        ReactGA.pageview(window.location.pathname + window.location.search);
     
    }

    switchTheme = () =>
    {

    }
    render = (props) => {
        const { classes } = this.props;
        const deadlines = { fontSize: "10pt", color: "red" };
        const appbar = { alignItems: 'center', width: '100%' , flexWrap:"wrap"};
        const toolbar = { alignItems: 'center', width: '100%' , flexWrap:"wrap"};
        const div_center = { textAlign: 'center'};
        const wide_centered_div = {width: "100%" };
        const root = { flexGrow: 1};
        const invisible = {visibility: "hidden"}
        return (
            <div className={root}>
                <AppBar position="static" color="default" className={appbar}>
                    <Toolbar className='top-menu' >
                            <div style={div_center}>
                                <Button color="primary" variant="contained" href="https://www.pavoterservices.pa.gov/Pages/VoterRegistrationApplication.aspx">REGISTER TO VOTE</Button>&nbsp;
                        <br /><span style={deadlines}>(deadline: October 19th)</span>
                            </div>


                            <div style={div_center}>
                                <Button color="primary" variant="contained" href="https://www.pavoterservices.pa.gov/OnlineAbsenteeApplication/#/OnlineAbsenteeBegin">SIGN UP TO VOTE BY MAIL</Button>&nbsp;
                        <br /><span style={deadlines}>(deadline: October 27th)</span>
                            </div>


                            <div style={div_center}>
                                <Button color="primary" variant="contained" href="https://twitter.com/PhillyVotes/status/1309231928080510976?s=20">HOW TO FILL OUT YOUR MAIL-IN BALLOT SO IT COUNTS</Button>&nbsp;
                        <br /><span style={deadlines}>(deadline: November 3rd)</span>
                            </div>

                            <div style={div_center}>
                                <Button color="primary" variant="contained" href="https://www.philadelphiavotes.com/en/home/item/1845-open_satellite_election_offices_on_a_rolling_basis?fbclid=IwAR0wa6ij_4ZMIUX9tRxfWwBD5VgAMSA49fxG_2zMVwUld8tfWBTMSTcXN_Y">PHILLY EARLY VOTING LOCATIONS</Button>&nbsp;
                        <br /><span style={deadlines}>(deadline: October 27th)</span>
                            </div>

                            <div style={div_center}>
                                <Button color="primary" variant="contained" onClick={this.switchTheme}>Democracy Mode</Button>&nbsp;
                        <br /><span style={invisible}> _</span>
                            </div>
                    </Toolbar>
                </AppBar>
                <center>
                    <br />
                    <Typography variant="h2" >VOTING BY MAIL:</Typography><br />
                    <Typography variant="h1" fontWeight="fontWeightBold">YOU CAN DO IT NAKED</Typography><br />
                    <Typography variant="h2" >BUT YOUR BALLOT CAN'T</Typography><br />
                    <Typography variant="h1" >
                        <span role="img">📄 ➡️ ✉️ ➡️ 📨 ➡️ 📬</span>
                    </Typography><br></br>
                    <a href="https://twitter.com/DavidABergstein/status/1309116828988379138?s=20">wut?</a>
                </center>
            </div>

        );
    }
}
export default Index;