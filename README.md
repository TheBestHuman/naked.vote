# Naked Vote 

## About

"Naked Vote" is a React application built in 2020 by Brian Kimmel, aimed at raising awareness about "Naked Ballots" in Pennsylvania. A naked ballot is a mail-in ballot that is at risk of being discarded if not sealed correctly in the provided security envelope. The objective of this project is to educate voters on how to properly seal their mail-in ballots to ensure their votes are counted.

You can access the application at [http://naked.vote](http://naked.vote).

## Features

1. **Informative content:** Guides the users through the process of correctly sealing and mailing their ballots to prevent them from becoming 'naked'.
2. **Engaging User Interface:** The UI is built with React, providing an intuitive and engaging experience for the user.
3. **State-specific information:** While focusing on the mail-in ballot rules for Pennsylvania, the site also provides a comprehensive guide for voters in other states.

## Getting Started

To get a local copy up and running, follow these simple steps.

### Prerequisites

- You will need to have `node` and `npm` installed on your machine. This project was built using `node v14.15.1` and `npm v6.14.8`. To install them, you can download Node.js [here](https://nodejs.org/).

### Installation

1. Clone the repo:
```
git clone https://gitlab.com/TheBestHuman/naked.vote.git
```
2. Go into the project folder:
```
cd naked.vote
```
3. Install the dependencies:
```
npm install
```
4. Start the local server:
```
npm start
```
5. Open the local app at:
```
http://localhost:3000
```

## Built With

- [React](https://reactjs.org/)
- [React Router](https://reactrouter.com/)
- [Styled Components](https://styled-components.com/)

## License

This project is Copyright © 2023 Brian Kimmel. All rights reserved.
